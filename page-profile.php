<?php
/*
 Template Name: Profile Page
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-all d-all cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<section class="entry-content cf" itemprop="articleBody">

									<?php the_field('profile_video'); ?>

									<?php // CV

									if( have_rows('profile_cv') ): ?>
										<div class="m-all t-all d-all content-box background-white cv">
											<h1>Peter Friis</h1>
										<?php 

										while( have_rows('profile_cv') ): the_row(); ?>
											<div>
												<?php if( get_sub_field('profile_cv_header') ): ?>
													<h2><?php the_sub_field('profile_cv_header'); ?></h2>
												<?php endif; 

												if( have_rows('profile_cv_entry') ): 

													while( have_rows('profile_cv_entry') ): the_row(); ?>
													<ul>
														<li>

															<?php if( get_sub_field('profile_cv_entry_year') ): ?>
																<span class="bold"><?php the_sub_field('profile_cv_entry_year'); ?></span>
															<?php endif; ?>

															<?php the_sub_field('profile_cv_entry_content'); ?>
														</li>
													</ul>
													<?php endwhile; 
												 endif; //if( get_sub_field('items') ): ?>
											</div>	

										<?php endwhile; // while( has_sub_field('to-do_lists') ): ?>
										</div>
									<?php endif; // if( get_field('to-do_lists') ): ?>

									<?php // Cases

									if( have_rows('profile_cases') ): ?>
										<div class="m-all t-all d-all content-box background-white;">
											<h1>Cases</h1>
										</div>

										<?php

									    while ( have_rows('profile_cases') ) : the_row(); ?>

									      <div class="m-all t-all d-all content-box <?php the_sub_field('profile_case_backgroundcolor'); if( get_sub_field('profile_case_collapse') ) { echo ' readmore'; }; ?>">

									      <?php the_sub_field('profile_case_content'); ?>

									      </div>

									  <?php
									    endwhile;

									else :

									    // no layouts found

									endif;

									?>


									<?php // References
									if( have_rows('profile_references') ): ?>
										<div class="m-all t-all d-all content-box background-white references">
											<h1>Referencer</h1>
										<?php 

										// loop through rows (parent repeater)
										while( have_rows('profile_references') ): the_row(); ?>

													<ul>
														<li>
															<span class="bold"><?php the_sub_field('profile_reference_company'); ?></span>

															<?php if( get_sub_field('profile_reference_service') ): ?>
																<?php the_sub_field('profile_reference_service'); ?>
															<?php endif; ?>
														</li>
													</ul>

										<?php endwhile; // while( has_sub_field('to-do_lists') ): ?>
										</div>
									<?php endif; // if( get_field('to-do_lists') ): ?>

									<div class="m-all t-all d-all content-box background-white;">
										<h1>Netværk</h1>
										<?php the_field('profile_network'); ?>
									</div>

									<?php the_content(); ?>

								</section>

							</article>

							<?php endwhile; else : ?>

									<article id="post-not-found" class="hentry cf">
											<header class="article-header">
												<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
											<section class="entry-content">
												<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</main>

				</div>

			</div>


<?php get_footer(); ?>
