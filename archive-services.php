<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-all d-all cf content-box" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<h1><?php echo post_type_archive_title(); ?></h1>

							<h3><?php the_field('introduction_services', 213); ?></h3>
							
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article">

								<a class="services-archive" href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">

									<?php the_post_thumbnail( 'bones-thumb-600' ); ?>
									<h2 class="entry-title"><?php the_title(); ?></h2>

								</a>

							</article>

							<?php endwhile; ?>

									<?php bones_page_navi(); ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry cf">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the archive.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</main>

				</div>

			</div>

<?php get_footer(); ?>
