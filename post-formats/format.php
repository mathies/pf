<?php

/* START CONVERSATIONS */

if ( 'conversations' == get_post_type() ) { ?>


              <article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">

                <?php the_field('conversation_video_short'); ?>

                <div class="content-intro m-all t-1of2 d-1of2">
                    <p class="type">Samtale</p>
                    <hr>
                    <h1><?php the_title(); ?></h1>
                    <p><?php the_field('conversation_person'); ?></p>
                    <p><?php the_field('conversation_person_title'); ?></p>
                </div>

                <?php // the_post_thumbnail('full'); ?>

<?php

// check if the flexible content field has rows of data
if( have_rows('conversation_boxes') ):

     // loop through the rows of data
    while ( have_rows('conversation_boxes') ) : the_row(); ?>

    <?php if( get_row_layout() == 'conversation_box' ): ?>

      <div class="m-all t-all d-all content-box <?php the_sub_field('conversation_box_backgroundcolor'); if( get_sub_field('conversation_box_collapse') ) { echo ' readmore'; }; ?>">

      <?php the_sub_field('conversation_box_content'); ?>

      <?php if( get_sub_field('conversation_box_print') ) { echo '<a class="print-link">Print</a>'; }; ?>

      </div>

    <?php elseif( get_row_layout() == 'conversation_box_images' ): ?>

    <div class="m-all t-all d-all content-box">

    <?php while ( have_rows('conversation_box_image') ) : the_row(); ?>

    <div class="m-all t-all d-1of3 content-box-images">
    <?php $image = get_sub_field('conversation_box_images_image');
      if( !empty($image) ): ?>
      <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
    <?php endif; ?>

      <p class="bold"><?php the_sub_field('conversation_box_images_title'); ?></p>
      <p><?php the_sub_field('conversation_box_images_author'); ?></p>

      </div>

    <?php endwhile; ?>

    </div>

  <?php
    endif;

    endwhile;

else :

    // no layouts found

endif;

?>

                <?php the_field('conversation_video_long');

/* START SERVICES */

} elseif ( 'services' == get_post_type() ) { ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemprop="blogPost" itemtype="http://schema.org/BlogPosting">

                <?php the_field('service_video'); ?>

                <div class="content-intro bw m-all t-1of2 d-1of2">
                    <p class="type">Ydelse</p>
                    <hr>
                    <h1><?php the_title(); ?></h1>
                </div>

                <?php // the_post_thumbnail('full'); ?>

<?php

// check if the flexible content field has rows of data
if( have_rows('service_boxes') ):

     // loop through the rows of data
    while ( have_rows('service_boxes') ) : the_row(); ?>

      <div class="m-all t-all d-all content-box <?php the_sub_field('service_box_backgroundcolor'); if( get_sub_field('service_box_collapse') ) { echo ' readmore'; }; ?>">

      <?php the_sub_field('service_box_content'); ?>

      </div>

  <?php
    endwhile;

else :

    // no layouts found

endif;

}

?>

                <section class="entry-content cf" itemprop="articleBody">
                  <?php
                    // the content (pretty self explanatory huh)
                    the_content();

                    /*
                     * Link Pages is used in case you have posts that are set to break into
                     * multiple pages. You can remove this if you don't plan on doing that.
                     *
                     * Also, breaking content up into multiple pages is a horrible experience,
                     * so don't do it. While there are SOME edge cases where this is useful, it's
                     * mostly used for people to get more ad views. It's up to you but if you want
                     * to do it, you're wrong and I hate you. (Ok, I still love you but just not as much)
                     *
                     * http://gizmodo.com/5841121/google-wants-to-help-you-avoid-stupid-annoying-multiple-page-articles
                     *
                    */
                    wp_link_pages( array(
                      'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'bonestheme' ) . '</span>',
                      'after'       => '</div>',
                      'link_before' => '<span>',
                      'link_after'  => '</span>',
                    ) );
                  ?>
                </section> <?php // end article section ?>


                <?php //comments_template(); ?>

              </article> <?php // end article ?>
