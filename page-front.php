<?php
/*
 Template Name: Front Page
*/
?>


<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-all d-all cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class( 'cf' ); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">

								<h1><?php the_field('biography_name'); ?></h1>
								<p><?php the_field('biography_title'); ?></p>

<!-- SLIDER -->
<p class="introduction-front"><?php the_field('introduction'); ?><p>
<div class="slider">
<?php
$args = array('post_type' => array ( 'conversations', 'services'));
$queryObject = new WP_Query($args);
// The Loop!
if ($queryObject->have_posts()) {
    ?>
    <ul class="slides">
	    <?php
	    while ($queryObject->have_posts()) {
	        $queryObject->the_post();
	        	    if( in_array( 'slider', get_field('posts_check') ) ) {

						if ( 'conversations' == get_post_type() ) {

						?>
									        <li>
									        	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'full' ); ?>
									        	<div class="slider-caption">
										        	<p class="type">Samtale</p>
										        	<hr>
										        	<h1><?php the_title(); ?></h1>
										        	<?php the_field('conversation_person'); ?>
										        </div></a>
									        </li>

						<?php

						} elseif ( 'services' == get_post_type() ) {
						?>
									        <li>
									        	<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( 'full' ); ?>
									        	<div class="slider-caption">
										        	<p class="type">Ydelse</p>
										        	<hr>
										        	<h1><?php the_title(); ?></h1>
										        </div></a>
									        </li>

						<?php
						}
	    } }

	    ?>
    </ul>
    <?php } ?>
</div>

<!-- CONVERSATIONS -->
<div class="conversations-container m-all t-all d-all">
<h3>Samtaler</h3>
<p class="introduction-front"><?php the_field('introduction_conversations', 206); ?><p>
<?php
$queryObject = new WP_Query( 'post_type=conversations&posts_per_page=4' );
// The Loop!
if ($queryObject->have_posts()) {
    ?>
    <ul>
	    <?php
	    while ($queryObject->have_posts()) {
	        $queryObject->the_post();
	       	if( in_array( 'grid', get_field('posts_check') ) )
			{
	        ?>
	        <li class="m-all t-1of3 d-1of3">
	        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large'); ?>
	        <p class="conversations-title"><?php the_title(); ?></p>
	        <p class="conversations-name"><?php the_field('conversation_person'); ?></p></a></li>
	    <?php
	    } }
	    ?>
    </ul>
    <div class="m-all t-all d-all see-all"><a href="<?php echo home_url(); ?>/samtaler">Se alle samtaler</a></div>
    <?php } ?>
</div>

<hr class="m-all t-all d-all">

<!-- SERVICES -->
<div class="services-container m-all t-all d-all">
<h3>Ydelser</h3>
<p class="introduction-front"><?php the_field('introduction_services', 213); ?><p>
<?php
$queryObject = new WP_Query( 'post_type=services&posts_per_page=4' );
// The Loop!
if ($queryObject->have_posts()) {
    ?>
    <ul>
	    <?php
	    while ($queryObject->have_posts()) {
	        $queryObject->the_post();
	       	if( in_array( 'grid', get_field('posts_check') ) )
			{
	        ?>
	        <li class="m-all t-1of3 d-1of3">
	        <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large'); ?>
	    	<p class="services-title"><?php the_title(); ?></p></a></li>
	    <?php
	    } }
	    ?>
    </ul>
    <div class="m-all t-all d-all see-all"><a href="<?php echo home_url(); ?>/ydelser">Se alle ydelser</a></div>
    <?php } ?>
</div>

								<section class="entry-content cf" itemprop="articleBody">
									<?php
										// the content (pretty self explanatory huh)
										the_content();

										/*
										 * Link Pages is used in case you have posts that are set to break into
										 * multiple pages. You can remove this if you don't plan on doing that.
										 *
										 * Also, breaking content up into multiple pages is a horrible experience,
										 * so don't do it. While there are SOME edge cases where this is useful, it's
										 * mostly used for people to get more ad views. It's up to you but if you want
										 * to do it, you're wrong and I hate you. (Ok, I still love you but just not as much)
										 *
										 * http://gizmodo.com/5841121/google-wants-to-help-you-avoid-stupid-annoying-multiple-page-articles
										 *
										*/
										wp_link_pages( array(
											'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'bonestheme' ) . '</span>',
											'after'       => '</div>',
											'link_before' => '<span>',
											'link_after'  => '</span>',
										) );
									?>
								</section> <?php // end article section ?>

								<footer class="article-footer cf">

								</footer>

								<?php comments_template(); ?>

							</article>

							<?php endwhile; endif; ?>

						</main>

				</div>

			</div>

<?php get_footer(); ?>
